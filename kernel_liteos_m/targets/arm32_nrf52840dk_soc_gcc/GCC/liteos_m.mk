LITEOSTOPDIR := ../../../
LITEOSTOPDIR := $(realpath $(LITEOSTOPDIR))

# Common
SRC_FILES     += $(wildcard $(LITEOSTOPDIR)/kernel/src/*.c) \
                 $(wildcard $(LITEOSTOPDIR)/kernel/src/mm/*.c) \
                 $(wildcard $(LITEOSTOPDIR)/components/cpup/*.c) \
                 $(wildcard $(LITEOSTOPDIR)/components/los_backtrace/*.c) \
                 $(wildcard $(LITEOSTOPDIR)/components/power/*.c) \
                 $(wildcard $(LITEOSTOPDIR)/utils/*.c) \
                 $(wildcard $(LITEOSTOPDIR)/kernel/arch/arm/cortex-m4/gcc/*.S)

INC_FOLDERS   += $(LITEOSTOPDIR)/utils \
                 $(LITEOSTOPDIR)/kernel/include \
                 $(LITEOSTOPDIR)/components/cpup \
                 $(LITEOSTOPDIR)/components/backtrace \
                 $(LITEOSTOPDIR)/components/power

#config files
INC_FOLDERS += $(LITEOSTOPDIR)/targets/arm32_nrf52840dk_soc_gcc/OS_CONFIG 

#third party related
INC_FOLDERS   += $(LITEOSTOPDIR)/../third_party_bounds_checking_function/include \
                 $(LITEOSTOPDIR)/../third_party_bounds_checking_function/src

SRC_FILES     += $(wildcard $(LITEOSTOPDIR)/../third_party_bounds_checking_function/src/*.c)

# NMSIS related
INC_FOLDERS   += $(LITEOSTOPDIR)/kernel/arch/arm/cortex-m4/gcc
#                 $(LITEOSTOPDIR)/kernel/arch/arm/cortex-m4/iar

SRC_FILES     += $(wildcard $(LITEOSTOPDIR)/kernel/arch/arm/cortex-m4/gcc/*.c)

INC_FOLDERS   += . \
                 $(LITEOSTOPDIR)/kernel/arch/include
