/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include "uart.h"
#include "task_sample.h"
#include "target_config.h"
#include "nrf_delay.h"

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* Initialize UART driver */
//  uint32_t ctr=0;

  uart_init();

#if 0
  while(1)
  {
    printf(" in main function, ctr:%lu\r\n", ctr);
    ctr++;
    nrf_delay_ms(5);
  }
#endif

  /* USER CODE BEGIN Init */
  RunTaskSample();
  while (1) {
  }
}
