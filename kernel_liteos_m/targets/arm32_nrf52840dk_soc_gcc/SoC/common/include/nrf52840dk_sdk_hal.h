
#ifndef __NRF52840DK_SDK_HAL_HAL_H__
#define __NRF52840DK_SDK_HAL_HAL_H__

#ifdef __cplusplus
 extern "C" {
#endif

#ifndef __WEAK
#define __WEAK	__attribute__((weak))
#endif

#ifdef __cplusplus
}
#endif

#endif /* __NRF52840DK_SDK_HAL_HAL_H__ */
